﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public float hp;
    void Start()
    {
        hp = 75f;
    }

    void Update()
    {
        if(hp <= 0)
        {
            FindObjectOfType<GameController>().enemies.Remove(this.gameObject);
            Destroy(this.gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.relativeVelocity.magnitude >= 5f)
        {
            hp -= 25f;
        }
    }
}
