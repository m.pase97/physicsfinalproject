﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    [SerializeField] private GameObject pauseUI;
    [SerializeField] private GameObject winUI;
    public List<GameObject> enemies;

    void Update()
    {
        if( enemies.Count <= 0)
        {
            FindObjectOfType<CannonBehaviour>().notInPause = false;
            winUI.SetActive(true);
            Time.timeScale = 0f;
        }
    }

    public void Pause()
    {
        FindObjectOfType<CannonBehaviour>().notInPause = false;
        pauseUI.SetActive(true);
        Time.timeScale = 0f;
    }

    public void Resume()
    {
        FindObjectOfType<CannonBehaviour>().notInPause = true;
        pauseUI.SetActive(false);
        Time.timeScale = 1f;
    }

    public void SceneLoader(int SceneIndex)
    {
        SceneManager.LoadScene(SceneIndex);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
