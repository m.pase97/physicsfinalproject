﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CannonBehaviour : MonoBehaviour
{
    [SerializeField] private GameObject cannonBall;
    [SerializeField] private GameObject fireSpot;
    [SerializeField] private Image powerImage;
    [SerializeField] private float rotSpeed = 20;
    private Rigidbody rb;
    private float force = 1000;
    private float power;
    private float init;
    private AudioManager am;
    public bool notInPause = true;

    void Start()
    {
        am = FindObjectOfType<AudioManager>();
        rb = GetComponent<Rigidbody>();
        power = 0;
        init = 0;
    }

    void Update()
    {
        if (notInPause)
        {

            if (Input.GetAxisRaw("Horizontal") != 0)
            {
                //muovi cannone orizzontalmente
                float rot = rotSpeed * Time.deltaTime * Input.GetAxisRaw("Horizontal");
                Quaternion rotMat = Quaternion.Euler(0f, rot, 0f);
                if (rb.rotation.y < -0.45f && Input.GetAxisRaw("Horizontal") < 0f)
                {
                    rotMat = Quaternion.Euler(0f, 0f, 0f);
                }
                if (rb.rotation.y > 0.45f && Input.GetAxisRaw("Horizontal") > 0f)
                {
                    rotMat = Quaternion.Euler(0f, 0f, 0f);
                }
                rb.MoveRotation(rb.rotation * rotMat);

            }

            if (Input.GetAxisRaw("Vertical") != 0)
            {
                //muovi cannone verticalmente
                float rot = rotSpeed * Time.deltaTime * -Input.GetAxisRaw("Vertical");
                Quaternion rotMat = Quaternion.Euler(rot, 0f, 0f);
                if (rb.rotation.x < -0.30f && Input.GetAxisRaw("Vertical") > 0f)
                {
                    rotMat = Quaternion.Euler(0f, 0f, 0f);
                }
                if (rb.rotation.x > 0.10f && Input.GetAxisRaw("Vertical") < 0f)
                {
                    rotMat = Quaternion.Euler(0f, 0f, 0f);
                }

                rb.MoveRotation(rb.rotation * rotMat);

            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                init = Time.time;
            }
            if (Input.GetKey(KeyCode.Space))
            {
                //spara!
                power = Mathf.Abs(Mathf.Sin(init - Time.time));
                powerImage.fillAmount = power;
            }
            if (Input.GetKeyUp(KeyCode.Space))
            {
                am.Play("FireSound");
                GameObject go = Instantiate(cannonBall, fireSpot.transform.position, Quaternion.identity);
                go.GetComponent<Rigidbody>().AddForce(fireSpot.transform.forward * force * power);
                rb.AddForceAtPosition(new Vector3(0,5,0), fireSpot.transform.position, ForceMode.Impulse);
            }
        }
    }
}
