﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBallBehaviour : MonoBehaviour
{
    public float explosionSpeed;

    public float radius;
    public float power;

    public GameObject explosion;
    public GameObject fire;

    private AudioManager am;

    void Start()
    {
        am = FindObjectOfType<AudioManager>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        Vector3 explosionPos = transform.position;

        am.Play("ExplosionSound");
        Instantiate(explosion, explosionPos, Quaternion.identity);
        if(collision.gameObject.tag == "Ground")
        {
            Instantiate(fire, explosionPos, Quaternion.identity);
        }

        Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);

        foreach(Collider c in colliders)
        {
            if(c.gameObject.tag == "Enemy")
            {
                c.gameObject.GetComponent<EnemyController>().hp -= 50;
            }

            Rigidbody rb = c.GetComponent<Rigidbody>();

            if(rb != null)
            {
                rb.AddExplosionForce(power, explosionPos, radius, 3.0f);
            }
        }

        Destroy(this.gameObject);
    }
}
